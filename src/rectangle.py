from quadrilateral import Quadrilateral
from two_d_point import TwoDPoint


class Rectangle(Quadrilateral):

    def __init__(self, *floats):
        super().__init__(*floats)
        if not self.__is_member():
            raise TypeError("A rectangle cannot be formed by the given coordinates.")

    def __str__(self):
        name = "Rectangle: "
        result = name + ", ".join(list(map(str, self.vertices)))
        return result

    def __eq__(self, other_shape):
        return type(other_shape) is Rectangle and self._vertices_are_equal(self.vertices, other_shape.vertices)

    def __is_member(self):
        """Returns True if the given coordinates form a valid rectangle, and False otherwise."""
        side_lengths = super().side_lengths()
        if (side_lengths[0] == side_lengths[2]) and (side_lengths[1] == side_lengths[3]):
            return self.__equal_length_diagonals()
        else:
            return False

    def __equal_length_diagonals(self):
        return self._calculate_distance(self.vertices[0], self.vertices[2]) == \
               self._calculate_distance(self.vertices[1], self.vertices[3])

    def center(self):
        """Returns the center of this rectangle, calculated to be the point of intersection of its diagonals."""
        m_1 = self.__calculate_slope(self.vertices[0], self.vertices[2])
        b_1 = self.__calculate_y_intercept(self.vertices[0], m_1)
        m_2 = self.__calculate_slope(self.vertices[1], self.vertices[3])
        b_2 = self.__calculate_y_intercept(self.vertices[1], m_2)
        x_intersection = (b_2 - b_1) / (m_1 - m_2)
        y_intersection = m_1 * x_intersection + b_1
        return TwoDPoint(x_intersection, y_intersection)

    def __calculate_slope(self, point_one, point_two):
        y_diff = point_two.y - point_one.y
        x_diff = point_two.x - point_one.x
        return y_diff / x_diff

    def __calculate_y_intercept(self, point, slope):
        return point.y - (point.x * slope)

    def area(self):
        """Returns the area of this rectangle. The implementation invokes the side_lengths() method from the superclass,
        and computes the product of this rectangle's length and width."""
        side_lengths = self.side_lengths()
        length = side_lengths[0]
        width = side_lengths[1]
        return length * width
