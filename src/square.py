from rectangle import Rectangle
from quadrilateral import Quadrilateral
from two_d_point import TwoDPoint


class Square(Rectangle):

    def __init__(self, *floats):
        super().__init__(*floats)
        if not self.__is_member():
            raise TypeError("A square cannot be formed by the given coordinates.")

    def __str__(self):
        name = "Square: "
        result = name + ", ".join(list(map(str, self.vertices)))
        return result

    def __eq__(self, other_shape):
        return type(other_shape) is Square and self._vertices_are_equal(self.vertices, other_shape.vertices)

    def __is_member(self):
        side_lengths = self.side_lengths()
        return self.__all_sides_equal(side_lengths)

    def __all_sides_equal(self, array):
        return len(set(array)) == 1

    def snap(self):
        """Snaps the sides of the square such that each corner (x,y) is modified to be a corner (x',y') where x' is the
        integer value closest to x and y' is the integer value closest to y. This, of course, may change the shape to a
        general quadrilateral, hence the return type. The only exception is when the square is positioned in a way where
        this approximation will lead it to vanish into a single point. In that case, a call to snap() will not modify
        this square in any way."""
        new_points = []
        for point in self.vertices:
            new_points.append(self.__get_closest_point(point))
        if self.__all_points_are_equal(new_points):
            return self.__quadrilateral_from_points(self.vertices)
        return self.__quadrilateral_from_points(new_points)

    def __get_closest_point(self, point):
        new_x = round(point.x)
        new_y = round(point.y)
        return TwoDPoint(new_x, new_y)

    def __all_points_are_equal(self, points):
        points_are_same = [points[0] == points[1],
                           points[1] == points[2],
                           points[2] == points[3],
                           ]
        return all(points_are_same)

    def __quadrilateral_from_points(self, points):
        floats = self.__get_floats_of_vertices(points)
        return Quadrilateral(floats[0], floats[1], floats[2], floats[3], floats[4], floats[5], floats[6], floats[7])

    def __get_floats_of_vertices(self, points):
        floats = []
        for point in points:
            floats.append(point.x)
            floats.append(point.y)
        return floats

