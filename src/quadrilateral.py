from two_d_point import TwoDPoint
import math
from typing import Tuple


class Quadrilateral:

    def __init__(self, *floats):
        points = TwoDPoint.from_coordinates(list(floats))
        self.__vertices = tuple(points[0:4])

    def __str__(self):
        name = "Quadrilateral: "
        result = name + ", ".join(list(map(str, self.__vertices)))
        return result

    def __eq__(self, other_shape):
        return type(other_shape) == Quadrilateral and self._vertices_are_equal(self.vertices, other_shape.vertices)

    def _vertices_are_equal(self, vertices_one, vertices_two):
        vertices_are_equal = True
        for i in range(len(vertices_one)):
            if vertices_one[i] != vertices_two[i]:
                vertices_are_equal = False
                break
        return vertices_are_equal

    @property
    def vertices(self):
        return self.__vertices

    def side_lengths(self):
        """Returns a tuple of four floats, each denoting the length of a side of this quadrilateral. The value must be
        ordered clockwise, starting from the top left corner."""
        side_lengths = []
        for i in range(len(self.__vertices)-1):
            side_lengths.append(self._calculate_distance(self.__vertices[i], self.__vertices[i+1]))
        side_lengths.append(self._calculate_distance(self.__vertices[3], self.__vertices[0]))
        return tuple(side_lengths)

    def _calculate_distance(self, vertex_one, vertex_two):
        x_dist = vertex_two.x - vertex_one.x
        y_dist = vertex_two.y - vertex_one.y
        return round(math.sqrt(x_dist**2 + y_dist**2), 9)

    def smallest_x(self):
        """Returns the x-coordinate of the vertex with the smallest x-value of the four vertices of this
        quadrilateral."""
        x_values = [vertex.x for vertex in self.__vertices]
        return min(x_values)
