class ShapeSorter:

    @staticmethod
    def sort(*shapes):
        shapes_list = list(shapes)
        shapes_list.sort(key=lambda shape: shape.smallest_x())
        return shapes_list

